let adminUser = localStorage.getItem("isAdmin");
let cardFooter;
let modalButton = document.querySelector("#adminButton");
let token = localStorage.getItem("token ")

if(adminUser == "false" || !adminUser) {

	modalButton.innerHTML = null;

} else {

	modalButton.innerHTML =
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">
				Add Course
			</a>
		</div>
	`

}

fetch('https://fathomless-fortress-13889.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {

	console.log(data);

	
	let courseData;

	if(data.length < 1) {

		courseData = "No courses available"

	} else {

		courseData = data.map(course => {
			
			if(adminUser === "true") {

			

					/**********
					  ADMIN
					 ********/
				
			 
				if (course.isActive === true) {
						cardFooter =
					`
					<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-info text-white btn-block viewButton">
							View
						</a>
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-warning text-white btn-block editButton">
							Edit
						</a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block disableButton">
							Disable Course
						</a>
					`
					} else {

						cardFooter =
						`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-info text-white btn-block viewButton">
								View
							</a>
							<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-warning text-white btn-block editButton">
								Edit
							</a>
							<a href="./enableCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-success text-white btn-block enableButton">
								Enable Course
							</a>
						`
					}return (
				`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									$ ${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
			)

			/***********
				 NOT ADMIN 
				 ********/ 

						} else {
				if (course.isActive === true) {
				cardFooter =
				`
					<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block selectButton">
						Select Course
					</a>
			
				`
				return (	
				`<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									$ ${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
			`	
			
		)} else {
			 return null;
		 };

						}	
				

			

			

		
		}).join("");

		

	}

	document.querySelector("#coursesContainer").innerHTML = courseData;

})
