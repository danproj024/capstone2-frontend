
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let token = localStorage.getItem('token');


	fetch(`https://fathomless-fortress-13889.herokuapp.com/api/courses/${courseId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            courseId: courseId,
           
        })
    })
    .then(res => res.json())
    .then(data => {
        
    	console.log(data);

    	if(data === true){

    		// Update course successful
    	    // Redirect to courses page
    	    window.location.replace("./courses.html");

    	}else{

    	    // Error in updating a course
    	    alert("something went wrong");

    	}

    })
