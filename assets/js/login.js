let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e) => {

	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	console.log(email);
	console.log(password);

	if ( email == "" || password == "" ) {

		alert("Please input your email and/or password.");

	} else {

		fetch('https://fathomless-fortress-13889.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data.accessToken) {

				// Store JWT in local storage
				localStorage.setItem('token', data.accessToken);

				fetch('https://fathomless-fortress-13889.herokuapp.com/api/users/details', {
					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					// Store the user's id and isAdmin properties in the local storage
					localStorage.setItem("id", data._id);
					localStorage.setItem("isAdmin", data.isAdmin);

					window.location.replace("./courses.html");

				})

			} else {

				alert("Something went wrong.");

			}

		})

	}

})

// localStorage {
// 	"token": "fajfj2oiun82u8uf28"
// }