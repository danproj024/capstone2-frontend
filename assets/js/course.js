let params = new URLSearchParams(location.search);
let courseId = params.get("courseId");
let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let admin = localStorage.getItem("isAdmin");
let token = localStorage.getItem("token");

/*****************
 USER COURSE PAGE 
 ******************/ 
  fetch(`https://fathomless-fortress-13889.herokuapp.com/api/courses/${courseId}`)
  .then((res) => res.json())
  .then((data) => {   
    
    console.log(data);
    
    courseName.innerHTML = data.name;
    courseDesc.innerHTML = data.description;
    coursePrice.innerHTML = data.price;
 
    if (admin === "false" || !admin ) {

      enrollContainer.innerHTML = 
      `<button id="enrollButton" class="btn btn-block btn-warning">
      Enroll
      </button>`;

      document.querySelector("#enrollButton").addEventListener("click", () => {
        
        fetch("https://fathomless-fortress-13889.herokuapp.com/api/users/enroll", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },

          body: JSON.stringify({
            courseId: courseId,
          }),

        })
          .then(res => res.json())
          .then(data => {
            
			if (data === true) {
              alert("Thank you for enrolling to the course.");
              window.location.replace("./courses.html");
            
			} else {
              alert("Something Went Wrong.");
            }
          });
          
      });
      /*****************
        ADMIN ENROLLEES PAGE 
        ******************/ 
     } else {
       fetch("https://fathomless-fortress-13889.herokuapp.com/api/users/")
          .then(res => res.json())
          .then(students => {
            let enrollees = data.enrollees;

            //console.log(enrollees);
            
              //MAP TESTING
            //const studentData = enrollees.map;
              //  students.map(student => {
              //    student.filter(studentData);
              //   console.log(student)


                  //FOR EACH TESTING
              // To bring out data from Course /enrollees / userId
            enrollees.forEach(studentData => {
              //console.log(studentData)
              
              //To bring out data from Users / enrollments / _id
               students.forEach(student => {
                 //console.log(student)
              
                   
               if (student._id === studentData.userId ) {

                  enrollContainer.innerHTML += 
                  
                `
                  <div class="card">
                  <div class="card-body">
                  <h3 class="card-title">${student.firstName} ${student.lastName}</h3>
                  </div>
                    </div>
                    `;
               }
               });
            });
          });
     };

    
        
     
  });
