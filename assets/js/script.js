let navItems = document.querySelector("#navSession");
let registerBtn = document.querySelector("#registerBtn");
let profileBtn = document.querySelector("#profileBtn");
let coursesBtn = document.querySelector('#coursesBtn');
let userToken = localStorage.getItem("token");
console.log(userToken);

if (!userToken) {

	navItems.innerHTML = 
		`
			<li class="nav-item">
				<a href="./login.html" class="nav-link"> Log In </a>
			</li>
		`

	registerBtn.innerHTML =
		`
			<li class="nav-item">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
		`


	
}else {

	coursesBtn.innerHTML = 
		`
		<li class="nav-item active">
		<a href="./courses.html" class="nav-link"> Courses </a>
		</li>
		`

		profileBtn.innerHTML = 
		
		`
		<li class="nav-item ">
		<a href="./profilePage.html" class="nav-link"> Profile </a>
		</li>
		`
		
		navItems.innerHTML = 
		`

			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
		`
	
}