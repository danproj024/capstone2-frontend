const firstName = document.querySelector(".firstName");
const lastName = document.querySelector(".lastName");
const mobileNo = document.querySelector(".mobileNo");
const email = document.querySelector(".email");
const enrollCourse = document.querySelector('#enrollCourse');
let token = localStorage.getItem('token');


/***************
 USER INFORMATION
 *****************/ 
//from router user.js
fetch('https://fathomless-fortress-13889.herokuapp.com/api/users/details', {
    headers: {
        'Authorization' : `Bearer ${token}`
    }
})
.then(res => res.json())
.then(data => {
    console.log(data)

    if (data) {
            firstName.innerHTML = data.firstName;
            lastName.innerHTML = data.lastName;
            mobileNo.innerHTML = data.mobileNo;
            email.innerHTML = data.email;
    } else {
        alert("Can't fetch data") 
    }

/*******************
COURSES INFORMATION
*********************/ 
    let enrollments = data.enrollments;

    enrollments.map((course) => {
        console.log(course)
        
        let courseId = course.courseId
        //from router course.js
    fetch(`https://fathomless-fortress-13889.herokuapp.com/api/courses/${courseId}`, {
    headers: {
        
          'Authorization' : `Bearer ${token}`
    }
    })
    .then(res => res.json())
    .then(data => {
        if (data){
            enrollCourse.innerHTML += 
            `
            <div class="d-flex justify-content-center">
            <div class="card bg-dark text-white" style="width: 16rem;">
            <div class="card-body ">
                <h5 class="card-title">${data.name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${course.enrolledOn}</h6>
                <h6 class="card-subtitle mb-2 text-muted-white">${course .status}</h6>   
            </div>
            </div>
            </div>
            `
        } else{
            alert("Can't fetch data")
        }
    }) 

});

    


})


// let token = localStorage.getItem("token");
// // console.log(token);

// let profileContainer = document.querySelector("#profileContainer");

// if(!token || token === null) {

// 	// Unauthenticated user
// 	alert('You must login first');
// 	// Redirect user to login page
// 	window.location.href="./login.html";

// } else {

// 	fetch('http://localhost:4000/api/users/details', {
// 		method: 'GET',
// 		headers: {
//             'Content-Type': 'application/json',
//             'Authorization': `Bearer ${token}`
//         },
// 	})
// 	.then(res => res.json())
// 	.then(data => {

// 		console.log(data);

// 		enrollCourse.innerHTML = 
// 			`
//             <div class="d-flex justify-content-center">
//                          <div class="card bg-dark text-white" style="width: 16rem;">
//                         <div class="card-body ">
//                              <h5 class="card-title">${data.name}</h5>
//                              <h6 class="card-subtitle mb-2 text-muted">${courseData.enrolledOn}</h6>
//                              <h6 class="card-subtitle mb-2 text-muted-white">${courseData .status}</h6>   
//                          </div>
//                          </div>
//                          </div>
// 			`

// 		let courses = document.querySelector("#courses");

// 		data.enrollments.forEach(courseData => {

// 			console.log(courseData);

// 			fetch(`http://localhost:4000/api/courses/${courseData.courseId}`)
// 			.then(res => res.json())
// 			.then(data => {

// 				courses.innerHTML += 
// 					`
// 						<tr>
// 							<td>${data.name}</td>
// 							<td>${courseData.enrolledOn}</td>
// 							<td>${courseData.status}</td>
// 						</tr>
// 					`

// 			})
		
// 		})

// 	})
// }




